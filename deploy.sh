#!/bin/sh
cd /Users/brianmills/workspace/sprint-reports
#rm -rf build
yarn build
aws s3 cp build s3://assets.marginedge.com/sprints/ --recursive
cd -
