import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import Grid from '@material-ui/core/Grid';


import '../node_modules/react-vis/dist/style.css';
import {XYPlot, LineMarkSeries, AreaSeries, HorizontalGridLines, XAxis, YAxis, DiscreteColorLegend, makeWidthFlexible, Hint, RadialChart} from 'react-vis';

const FlexibleXYPlot = makeWidthFlexible(XYPlot); 

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

class EpicLineChart extends Component {
  state = {
    hintData: false
  }
  render() {
    
    const {hintData} = this.state;

    // transform into data

    const chartData = [];
    let versions = [];
    
    let epics = [];
    if (this.props.sprintEpics) { 
      this.props.sprintEpics.forEach(t => {
        if (!epics.includes(t.epicName)) {
          epics.push(t.epicName);
        }

        if (!versions.includes(t.version)) {
          versions.push(t.version);
        }
      });
    }

    let epicLegendData = [];
    epics.forEach(e => {
      epicLegendData.push({title: e});
      let epicData = {
        points: [],
        name: e
      };

      versions = versions.sort(function(a,b){ return a-b; });
      versions.forEach(v => {
        let epicEffort = 0;
        this.props.sprintEpics.forEach(t => {
          if (t.version === v && t.epicName === e) {
            epicEffort = t.estimate;
          }
        });
        epicData.points.push({x: v, y: epicEffort, e: e});
      });

      // for now manually skip a few epics
      if (epicData.name !== 'Nerd Stuff' && epicData.name !== 'Bugs') {
        chartData.push(epicData);
      }
      
    });

    const chartVersions = versions;
    const ITEMS = epicLegendData;

    return (
        <Grid container spacing={8}>
          <Grid item md={9}>

            <FlexibleXYPlot height={600} xType="ordinal" xDomain={chartVersions}>
            <HorizontalGridLines />
            <XAxis title="Sprint" />
            <YAxis title="Effort"/>
            {chartData.map(d=>
                <LineMarkSeries 
                  key={d.name} 
                  data={d.points} 
                  onValueMouseOut={ v=>this.setState({hintData: false}) } 
                  onValueClick={ v => this.setState({hintData: v.x && v.y ? v : false}) }
                  onValueMouseOver={ v => this.setState({hintData: v.x && v.y ? v : false}) } />
              )}

            {hintData ? <Hint value={hintData} style={{color: 'black'}}>
              <div>
                <p>{hintData.e}</p>
              </div>
            </Hint> : null}
            </FlexibleXYPlot>
          </Grid>
          <Grid item md={3}>
            <DiscreteColorLegend items={ITEMS} />
          </Grid>
        </Grid>
    );
    }
}

class EpicPieChart extends Component {
  state = {
  }
  render() {
    
    return (
        <Grid container spacing={8}>
          <Grid item align="center" md={9}>
          <RadialChart
            data={this.props.epicTotals}
            width={600}
            height={600} 
            labelsRadiusMultiplier={.9}
            showLabels={true} 
            labelsAboveChildren={true} />
          </Grid>
        </Grid>
    );
    }
}

class SprintEffortTotals extends Component {

  render() {

    // build a map of version to # of hours in the version
    if (!this.props.sprintEffortTotals) {
      return (<div>'Loading'</div>);
    } 
    else {

      return (
        <Paper>
          <Table>
            <TableHead>
              <TableRow><TableCell>Version</TableCell><TableCell numeric>Hours</TableCell><TableCell numeric>Complete</TableCell></TableRow>
            </TableHead>
            <TableBody>
             {this.props.sprintEffortTotals.map(t =>
                  <TableRow key={t.version}><TableCell>{t.version}</TableCell><TableCell numeric>{t.total}</TableCell><TableCell numeric>{t.complete}</TableCell></TableRow>
              )}
            </TableBody>
          </Table>
        </Paper>
      );
    }
  }
}


class SprintEpicTotals extends Component {
  render() {
    // build a map of version to # of hours in the version
    if (!this.props.sprintEpics) {
      return (<div>'Loading'</div>);
    } 
    else {
      const localData = this.props.sprintEpics.filter(s => s.percent > 5);
      return (
        <Paper>
        <Table>
          <TableHead>
            <TableRow><TableCell>Version</TableCell><TableCell>Epic Key</TableCell><TableCell>Epic Name</TableCell><TableCell numeric>Estimate</TableCell><TableCell numeric>Percent</TableCell></TableRow>
          </TableHead>
          <TableBody>
          {localData.map(s => 
              <TableRow key={s.version+'-'+s.epicKey}><TableCell>{s.version}</TableCell><TableCell>{s.epicKey}</TableCell><TableCell>{s.epicName}</TableCell><TableCell numeric>{s.estimate}</TableCell><TableCell numeric>{s.percent+'%'}</TableCell></TableRow>
          )}
          </TableBody>
        </Table>
        </Paper>
      );
    }
  }
}


class SprintDevTotals extends Component {
  render() {
    // build a map of version to # of hours in the version
    if (!this.props.devEstimates) {
      return (<div>'Loading'</div>);
    } 
    else {

      // transform devEstimates into the most recent 4 versions only
      let versions = [];
      let developers = [];
      this.props.devEstimates.forEach(est => {
        if (!versions.includes(est.version)) {
          versions.push(est.version);
        }
        if (!developers.includes(est.name)) {
          developers.push(est.name);
        }
      });
      // show only the last 3 versions at most
      // filter out versions we haven't started yet
      let notStartedVersions = ['8.8', '8.7', '8.6', '8.5', '8.4'];
      versions = versions.filter(function(a) { return !notStartedVersions.includes(a); });
      versions = versions.sort(function(a,b){ return a-b; });
      versions = versions.slice(versions.length - 4, versions.length);
      developers = developers.sort(function(a,b){ return a-b; });
      
      let displayData = [];
      let idx = 0;
      versions.forEach(v => {
        displayData[idx] = [];
        developers.forEach(d => {
          this.props.devEstimates.forEach(est => {
            if (est.name === d && est.version === v) {
              displayData[idx].push(est);
            }
          });
          // if we didn't find the developer show a zero
          let addedDev = displayData[idx].find(data => { return data.name === d && data.version === v});
          if (!addedDev) {
            displayData[idx].push({name:d, version:v, estimate: 0});
          }
        });
        idx++;
      });

      // round the estimates
      displayData.forEach(dd => {
        dd.forEach(devData => {
          if (devData) {
            if (devData.estimate) {
              devData.estimate = Math.round(devData.estimate * 100) / 100;
            }
            if (!devData.avg) {
              devData.avg = 0.0;
            }
          }
        });
      });

      let tables=[];
      for(let i=0; i<displayData.length; i++) {
        tables.push(
            <Grid key={i} item md={6} >
            <Paper >
              <Table>
                <TableHead>
                  <TableRow><TableCell>Version</TableCell><TableCell>Dev</TableCell><TableCell numeric>Avg</TableCell><TableCell numeric>Estimate</TableCell></TableRow>
                </TableHead>
                <TableBody>
                {displayData[i].map(s => 
                    <TableRow key={s.version+'-'+s.name}><TableCell>{s.version}</TableCell><TableCell>{s.name}</TableCell><TableCell numeric>{s.avg}</TableCell><TableCell numeric>{s.estimate}</TableCell></TableRow>
                )}
                </TableBody>
              </Table>
            </Paper>
            </Grid> 
        );
      }

      return (
        <Grid container spacing={24}>
          {tables}
        </Grid> 
      );
    }
  }
}

class SprintDefectRate extends Component {
  render() {
    // build a map of version to # of hours in the version
    if (!this.props.devDefects) {
      return (<div>'Loading'</div>);
    } 
    else {

      return (
        <Paper>
        <Table>
          <TableHead>
            <TableRow><TableCell>Version</TableCell><TableCell>Developer</TableCell><TableCell numeric>Tickets</TableCell><TableCell numeric>Defects</TableCell><TableCell numeric>Hours</TableCell><TableCell numeric>Defects Per Hour</TableCell></TableRow>
          </TableHead>
          <TableBody>
          
          {this.props.devDefects.map(s => 
              <TableRow key={s.version+'-'+s.name}><TableCell>{s.version}</TableCell><TableCell>{s.name}</TableCell><TableCell numeric>{s.tickets}</TableCell><TableCell numeric>{s.defects}</TableCell><TableCell numeric>{s.estimate}</TableCell><TableCell numeric>{s.ratePerHour}</TableCell></TableRow>
          )}
          </TableBody>
        </Table>
        </Paper>
      );
    }
  }
}


class SprintDefectChart extends Component {
  state = {
    hintData: false
  }
  render() {
    
    const {hintData} = this.state;

    // transform into data

    let versions = [];    
    let developers = [];

    if (this.props.devDefects) { 
      this.props.devDefects.forEach(defect => {
        if (!developers.includes(defect.name)) {
          developers.push(defect.name);
        }
        
        if (!versions.includes(defect.version)) {
          versions.push(defect.version);
        }
      });
    }

    let series = [];
    let devLegendData = [];
    developers.forEach(dev => {
      devLegendData.push({title: dev});

      let devSeries = {
        points: [],
        name: dev
      };

      versions = versions.sort(function(a,b){ return a-b; });
      versions.forEach(v => {
        this.props.devDefects.forEach(defect => {
          if (defect.name === dev && defect.version === v) {
            devSeries.points.push({x: defect.version, y: defect.ratePerHour, name: dev});
          }
        });
      });
      series.push(devSeries);
    });

    const chartVersions = versions;
    const ITEMS = devLegendData;

    return (
        <Grid container spacing={8}>
          <Grid item md={9}>

            <FlexibleXYPlot height={600} xType="ordinal" xDomain={chartVersions}>
            <HorizontalGridLines />
            <XAxis title="Sprint" />
            <YAxis title="Defect Rate Per Hour"/>
            {series.map(d=>
                <LineMarkSeries 
                  key={d.name} 
                  data={d.points} 
                  onValueMouseOut={ v => this.setState({hintData: false}) } 
                  onValueClick={ v => { this.setState({hintData: v.x && v.y ? v : false}); } }
                  onValueMouseOver={ v => { this.setState({hintData: v.x && v.y ? v : false}); } } />
              )}

            {hintData ? <Hint value={hintData} style={{color: 'black'}}>
              <div>
                <p>{hintData.name}</p>
              </div>
            </Hint> : null}
            </FlexibleXYPlot>
          </Grid>
          <Grid item md={3}>
            <DiscreteColorLegend items={ITEMS} />
          </Grid>
        </Grid>
    );
    }
}

class SprintIssueTypeChart extends Component {
  state = {
    hintData: false
  }
  render() {
    
    const {hintData} = this.state;

    // transform into data

    let versions = [];    
    let types = [];

    if (this.props.devIssueTypes) { 
      this.props.devIssueTypes.forEach(issueData => {
        if (!types.includes(issueData.type)) {
          types.push(issueData.type);
        }
        
        if (!versions.includes(issueData.version)) {
          versions.push(issueData.version);
        }
      });
    }

    let series = [];
    let typeLegendData = [];
    types.forEach(t => {
      typeLegendData.push({title: t});

      let typeSeries = {
        points: [],
        name: t
      };

      versions = versions.sort(function(a,b){ return a-b; });
      versions.forEach(v => {

        this.props.devIssueTypes.forEach(issueData => {
          if (issueData.type === t && issueData.version === v) {
            typeSeries.points.push({x: issueData.version, y: issueData.percent, name: t});
          }
        });

        // if we didn't have the type in the version at all we fake a zero
        var addedPoint = typeSeries.points.find(point => { return point.name === t && point.x === v});
        if (!addedPoint) {
          typeSeries.points.push({x: v, y: 0, name: t});
        }
        
      });
      series.push(typeSeries);
    });

    series = series.sort(function(a, b){
        return b.name - a.name ;
      });

    const chartVersions = versions;
    const ITEMS = typeLegendData;

    return (
        <Grid container spacing={8}>
          <Grid item md={9}>

            <FlexibleXYPlot height={600} xType="ordinal" xDomain={chartVersions} stackBy="y">
            <HorizontalGridLines />
            <XAxis title="Sprint" />
            <YAxis title="Effort"/>
            {series.map(d=>
                <AreaSeries 
                  key={d.name} 
                  data={d.points} 
                  onValueMouseOut={ v => this.setState({hintData: false}) } 
                  onValueClick={ v => { this.setState({hintData: v.x && v.y ? v : false}); } }
                  onValueMouseOver={ v => { this.setState({hintData: v.x && v.y ? v : false}); } } />
              )}

            {hintData ? <Hint value={hintData} style={{color: 'black'}}>
              <div>
                <p>{hintData.name}</p>
              </div>
            </Hint> : null}
            </FlexibleXYPlot>
          </Grid>
          <Grid item md={3}>
            <DiscreteColorLegend items={ITEMS} />
          </Grid>
        </Grid>
    );
    }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      issues: [],
      epics: [],
      sprintEffortTotals: [],
      sprintEpics: [],
      devEstimates: [],
      devDefects: [],
      devIssueTypes: [],
      epicTotals: []
    };
    this.classes = styles;
  }

  render() {
    const { sprintEffortTotals, sprintEpics, devEstimates, devDefects, devIssueTypes, epicTotals } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">MarginEdge Sprint Reports</h1>
        </header>
        <div> 

        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={this.classes.heading}>Epic Line Chart</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Paper style={{width: '100%'}}>
              <EpicLineChart sprintEpics={sprintEpics}/>
            </Paper>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel defaultExpanded={true}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={this.classes.heading}>Recent Development Priorities</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Paper style={{width: '100%'}}>
              <EpicPieChart epicTotals={epicTotals}/>
            </Paper>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={this.classes.heading}>Issue Type Chart</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <SprintIssueTypeChart devIssueTypes={devIssueTypes}/>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel defaultExpanded={true}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={this.classes.heading}>Sprint Effort</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <SprintEffortTotals sprintEffortTotals={sprintEffortTotals} />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={this.classes.heading}>Sprint Focus</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <SprintEpicTotals sprintEpics={sprintEpics} />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel defaultExpanded={true}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={this.classes.heading}>Resource Allocation</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <SprintDevTotals devEstimates={devEstimates} />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={this.classes.heading}>Defect Report</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <SprintDefectRate devDefects={devDefects} />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel defaultExpanded={true}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={this.classes.heading}>Defect Chart</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <SprintDefectChart devDefects={devDefects} />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        </div>
      </div>
    );
  }

  componentDidMount() {

    var epicRequest = fetch('http://assets.marginedge.com/project/epics.json')
          .then(response => response.json())
          .then(data => this.setState({epics: data}));

    var issueRequest = fetch('http://assets.marginedge.com/project/issues.json')
      .then(response => response.json())
      .then(data => this.setState({issues: data}));

    Promise.all([issueRequest,epicRequest]).then(values => {

      let totalData = {};
      let completeTotalData = {};
      this.state.issues.forEach(i => {
        if (i.fields.fixVersions && i.fields.fixVersions.length > 0 && i.fields.timeoriginalestimate) {
          let fixVersion = i.fields.fixVersions[0].name;
          let estimate = i.fields.timeoriginalestimate;
          if (!(fixVersion in totalData)) {
            totalData[fixVersion] = 0;
          }
          totalData[fixVersion] += estimate/60/60;
          if (i.fields && i.fields.status && (i.fields.status.name === "Done" || i.fields.status.name === "Ready To Test" || i.fields.status.name === "In Progress")) {
            let progressGuess = estimate;
            if (i.fields.status.name === "Ready To Test") {
              // ready to test is considered 90% complete
              progressGuess = estimate * 0.9;
            } else if (i.fields.status.name === "In Progress") {
              progressGuess = estimate * 0.5;
            }
            if (!(fixVersion in completeTotalData)) {
              completeTotalData[fixVersion] = 0;
            }
            completeTotalData[fixVersion] += progressGuess/60/60;
          }
        }
      });
      let totals = [];
      Object.keys(totalData).forEach(t => {
        let complete = completeTotalData[t] || 0;
        totals.push({'version': t, 'total': totalData[t].toFixed(0), 'complete': complete.toFixed(0)});
      });

      this.setState({sprintEffortTotals: totals.sort(function(a, b){
        return b.version - a.version ;
      })});

      // generate epic key to name lookup
      let epicData = {};
      this.state.issues.forEach(i => {
        let epicKey = i.fields.customfield_10008;
        if (epicKey) {
          let epicName = '';
          this.state.epics.forEach(function (e) {
              if (e.key === epicKey) {
                epicName = e.fields.customfield_10009;
              }
          });

          epicData[epicKey] = epicName;
        }
      });
      this.setState({epicData: epicData});

      // calculate the % of time spent on each epic in each version
      let epicEffortData = {};
      let devEffortData = {};
      let devDefectData = {};
      let devTicketData = {};
      let devIssueTypeData = {}
      let versions = [];
      let devNames = [];
      this.state.issues.forEach(i => {
        if (i.fields.fixVersions && i.fields.fixVersions.length > 0 && i.fields.timeoriginalestimate) {
          let fixVersion = i.fields.fixVersions[0].name;
          let epicKey = i.fields.customfield_10008;
          let estimate = i.fields.timeoriginalestimate;
          let issueType = i.fields.issuetype.name;
          if (!versions.includes(fixVersion)) {
            versions.push(fixVersion);
          }


          let assigneeKey = fixVersion + "~Unassigned";
          if (i.fields.assignee) {
            assigneeKey = fixVersion + "~" + i.fields.assignee.displayName;
            if (!devNames.includes(i.fields.assignee.displayName)) {
              devNames.push(i.fields.assignee.displayName);
            }
          }
          if (!(assigneeKey in devEffortData)) {
            devEffortData[assigneeKey] = 0;
          }
          devEffortData[assigneeKey] += estimate/60/60;

          let issueTypeKey = fixVersion + "~" + issueType;
          if (!(issueTypeKey in devIssueTypeData)) {
            devIssueTypeData[issueTypeKey] = 0;
          }
          devIssueTypeData[issueTypeKey] += estimate/60/60;

          if (epicKey) {
            let key = fixVersion + "~" + epicKey;

            if (!(key in epicEffortData)) {
              epicEffortData[key] = 0;
            }
            epicEffortData[key] += estimate/60/60;

            // I am only tracking defects for projects linked to epics

            if (!(assigneeKey in devTicketData)) {
              devTicketData[assigneeKey] = 0;
            }
            devTicketData[assigneeKey] += 1;

            // calculate defect rate for developer
            if (i.fields.issuelinks && i.fields.issuelinks.length > 0) {
              i.fields.issuelinks.forEach(link => {
                let defectKey = (link.inwardIssue && link.inwardIssue.key) || (link.outwardIssue && link.outwardIssue.key);
                if (defectKey) {
                  // check to make sure it is really a defect
                  this.state.issues.forEach(defect => {
                    if (defect && 
                        defect.key === defectKey && 
                        defect.fields.issuetype.name === "Bug" &&
                        // in order to count it should be unresolved, fixed, or done
                        (!defect.fields.resolution || 
                          defect.fields.resolution.name === "Fixed" || 
                          defect.fields.resolution.name === "Done")) {

                      if (!(assigneeKey in devDefectData)) {
                        devDefectData[assigneeKey] = 0;
                      }
                      devDefectData[assigneeKey] += 1;
                    }
                  });
                }
              });
            }
          }
        }
      });

      // make sure the devEffortData map has a value for each dev even if they didn't participate in the sprint
      versions.forEach(v => {
        devNames.forEach(d => {
          if (!devEffortData[v+'~'+d]) {
            devEffortData[v+'~'+d] = 0;
          }
        });
      });

      // now build a data structure I can display to get a list of epic totals per verisons
      let sprintEpics = [];
      versions.forEach(v => {
        // add in the epic and effort
        Object.entries(epicEffortData).forEach(effort => {

          let key = effort[0];
          let version = key.split('~')[0];
          let epicKey = key.split('~')[1];
          let epicName = epicData[epicKey];
          let estimate = effort[1];
          // calculate % of version
          let totalVersionEstimate = totalData[version];
          let percentOfTotal = (estimate/totalVersionEstimate * 100).toFixed(1);
          if (v === version) {
            sprintEpics.push({
              version: version, epicKey: epicKey, epicName: epicName, estimate: estimate, percent: percentOfTotal
            });
          }
        });
      });
      sprintEpics.sort(function(a, b){
        return b.version - a.version || b.estimate - a.estimate;
      });
      this.setState({sprintEpics: sprintEpics});

      let recentCompletedVersions = ['7.9','8.0','8.1','8.2'];
      // now transform the epics into totals for completed versions
      // TODO maybe just do recent?
      let totalEpicEffort = 0;

      let epicTotals = [];
      Object.entries(epicData).forEach(epic => {
        let key = epic[0];
        let name = epic[1];
        let effort = 0;
        sprintEpics.forEach(se => {
          if (key === se.epicKey && recentCompletedVersions.includes(se.version)) {
            effort += se.estimate;
            totalEpicEffort += se.estimate;
          }
        });

        epicTotals.push({
          key: key,
          label: name,
          angle: effort
        });
      });

      // now transform the epic totals by filtering out the cruft into an "other" epic
      // for now I am just picking any epic under 

      let otherEpic = {
        key: "ME-0000",
        label: "Other",
        angle: 0
      };

      let itemsToRemove = [];
      epicTotals.forEach(et => {
        if (et.angle < 35) {
          otherEpic.angle += et.angle
          itemsToRemove.push(et);
        }
      })

      itemsToRemove.forEach(et => {
          for (var i=0; i < epicTotals.length; i++) {
            if(epicTotals[i].key === et.key) {
              epicTotals.splice(i,1);
            }
          }
      });

      epicTotals.push(otherEpic);

      epicTotals.sort(function(a, b){
        return b.angle - a.angle;
      });

      epicTotals.forEach(et => {
        et.subLabel = ((et.angle / totalEpicEffort) * 100).toFixed(1) + "%";
      })

      this.setState({epicTotals: epicTotals});
      this.setState({totalEpicEffort: totalEpicEffort});


      let devEstimates = [];

      // we want to calculate the average hours per developer per version
      // but I only want to include data from completed versions
      // no real good way right now to know which versions are complete so 
      // i am just going to keep a list


      let esimatesPerDev = {};

      versions.forEach(v => {
        Object.entries(devEffortData).forEach(effort => {
          let key = effort[0];
          let version = key.split('~')[0];
          let name = key.split('~')[1];
          let estimate = effort[1];
          if (v === version && recentCompletedVersions.find(v2 => { return v2 === v; })) {
            if (!esimatesPerDev.hasOwnProperty(name)) {
              esimatesPerDev[name] = [];
            }
            esimatesPerDev[name].push(estimate);
          }
        });
      });
      var average = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;
      Object.entries(esimatesPerDev).forEach(dev => {
        esimatesPerDev[dev[0]] = average(dev[1]).toFixed(0);
      });


      versions.forEach(v => {
        Object.entries(devEffortData).forEach(effort => {
          let key = effort[0];
          let version = key.split('~')[0];
          let name = key.split('~')[1];
          let estimate = effort[1];
          if (v === version) {
            devEstimates.push({
              name: name,
              version: version,
              estimate: estimate,
              avg: esimatesPerDev[name]
            });
          }
        });
      });
      devEstimates.sort(function(a, b){
        return b.version - a.version || b.estimate - a.estimate;
      });
      this.setState({devEstimates: devEstimates});

      // defect rate per version
      let devDefects = [];
      versions.forEach(v => {
        Object.entries(devTicketData).forEach(ticketData => {
          let key = ticketData[0];
          let version = key.split('~')[0];
          let name = key.split('~')[1];
          let tickets = ticketData[1];
          let defects = devDefectData[key] || 0;
          let estimate = devEffortData[key] || 0;
          if (v === version) {
            devDefects.push({
              name: name,
              version: version,
              tickets: tickets,
              defects: defects,
              estimate: estimate,
              rate: (defects/tickets),
              ratePerHour: (defects/estimate).toFixed(2)
            });
          }
        });
      });

      devDefects.sort(function(a, b){
        return b.version - a.version || b.ratePerHour - a.ratePerHour;
      });
      // temp filter to only 6.1
      devDefects = devDefects.filter(function(a) { return a.name !== 'Unassigned'; });
      this.setState({devDefects: devDefects});


      let devIssueTypes = [];
      versions.forEach(v => {
        Object.entries(devIssueTypeData).forEach(issueTypeData => {
          let key = issueTypeData[0];
          let version = key.split('~')[0];
          let type = key.split('~')[1];
          let estimate = issueTypeData[1] || 0;
          if (v === version) {
            let totalVersionEstimate = totalData[version];
            let percentOfTotal = (estimate/totalVersionEstimate * 100);
            devIssueTypes.push({
              type: type,
              version: version,
              estimate: estimate,
              percent: percentOfTotal
            });
          }
        });
      });

      devIssueTypes.sort(function(a, b){
        return a.version - b.version || a.type - b.type;
      });
      this.setState({devIssueTypes: devIssueTypes});
    });
  }
}

export default App;
